import sqlite3
import pandas as pd
from utils.result_wrappers import SearchRecord, ConfirmRecord, AdditionRecord


class Model:
    DB_PATH = "collector/blockchain_metrics.db"

    def __init__(self):
        self.con = sqlite3.connect(Model.DB_PATH)
        self.cur = self.con.cursor()
        self._create_tables()

    def insert_confirm(self, record: ConfirmRecord):
        self.cur.execute(
            "INSERT OR IGNORE INTO confirms VALUES(?,?,?,?,?,?)", (record.tx_id, record.duration, record.fee,
                                                                   record.timestamp, record.block_height,
                                                                   record.mempool))

    def insert_search(self, record: SearchRecord):
        self.cur.execute(
            "INSERT OR IGNORE INTO searches VALUES(?,?,?)", (record.coin_name, record.duration, record.timestamp))

    def insert_addition(self, record: AdditionRecord):
        self.cur.execute(
            "INSERT OR IGNORE INTO additions VALUES(?,?,?, ?)",
            (record.coin_name_new, record.coin_name_old, record.duration, record.mempool))

    def _create_tables(self):
        self.cur.execute("""CREATE TABLE IF NOT EXISTS confirms(
                            transaction_id INTEGER PRIMARY KEY,
                            duration REAL,
                            fee INTEGER,
                            created_timestamp INTEGER,
                            confirmed_block INTEGER,
                            mempool INTEGER)""")

        self.cur.execute("""CREATE TABLE IF NOT EXISTS searches(
                                    coin_id INTEGER PRIMARY KEY,
                                    duration REAL,
                                    response_timestamp INTEGER)""")

        self.cur.execute("""CREATE TABLE IF NOT EXISTS additions(
                                            coin_id_new INTEGER PRIMARY KEY,
                                            coin_id_old INTEGER,
                                            duration REAL,
                                            mempool INTEGER)""")

    def get_confirm_df(self):
        return pd.read_sql_query("SELECT * FROM confirms", self.con)

    def get_search_df(self):
        return pd.read_sql_query("SELECT * FROM searches", self.con)

    def get_addition_df(self):
        return pd.read_sql_query("SELECT * FROM additions", self.con)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cur.close()
        if isinstance(exc_val, Exception):
            self.con.rollback()
        else:
            self.con.commit()
        self.con.close()
