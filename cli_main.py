import asyncio
import click
import logging

from pathlib import Path
from functools import wraps

from chia.types.blockchain_format.coin import Coin
from chia.types.blockchain_format.sized_bytes import bytes32
from chia.types.coin_record import CoinRecord
from chia.types.spend_bundle import SpendBundle
from chia.util.ints import uint64
from chia.util.config import load_config
from chia.util.default_root import DEFAULT_ROOT_PATH
from chia.wallet.transaction_record import TransactionRecord
from chia.util.bech32m import decode_puzzle_hash

from connector import Rpc

from contracts.drivers.helpers.sign_contribution import AggregatedSignature
from contracts.drivers.helpers.piggy_spend import PiggySpend

current_net = "xch"
# Important note: chia wallets randomize their wallet addresses after transaction, to prevent
# randomized piggybank puzzlehashes (we want to keep track of them), we will hard code cash out
# puzzlehash (can be wallet, coin, etc..).
# <TODO Put your wallet address here:>
cash_out_address = "xch1u06mrmypa54jhyh7zwz6hy6cl0uvxgu8uze5yaj3jmwp9hq52mlsguptqk"
# <TODO End wallet address>
cash_out_puzzlehash: bytes32 = decode_puzzle_hash(cash_out_address)

chia_config = load_config(Path(DEFAULT_ROOT_PATH), "config.yaml")
hostname = chia_config["farmer"]["full_node_peer"]["host"]
node_port = chia_config["full_node"]["rpc_port"]
wallet_port = chia_config["wallet"]["rpc_port"]


def coroutine(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        return asyncio.run(function(*args, **kwargs))

    return wrapper


@click.group()
def cli():
    pass


@cli.command(help="Search coin record by puzzlehash")
@click.argument("puzzle_hash")
@coroutine
async def search_coin(puzzle_hash: bytes32):
    async with await Rpc.create_connection(hostname, node_port, wallet_port, DEFAULT_ROOT_PATH, chia_config) as rpc:
        logging.info(f"Searching coin with id '{puzzle_hash}':")
        coin_record = await rpc.full_node_client.get_coin_records_by_puzzle_hash(bytes.fromhex(puzzle_hash))
        if coin_record is None:
            logging.info("Couldn't find coin record with this id.")
        else:
            logging.info(f"Coin record:\n{coin_record}")


async def get_objects(rpc: Rpc, fee: uint64, piggybank_goal: uint64):
    wallet_fingerprint = await rpc.wallet_client.get_logged_in_fingerprint()
    # wallet_fingerprint = 1881364676
    keys = await rpc.wallet_client.get_private_key(wallet_fingerprint)
    public_key = keys['pk']
    private_key = keys['sk']
    aggregator = AggregatedSignature(private_key, public_key)
    piggy_spend = PiggySpend(rpc=rpc,
                             aggregator=aggregator,
                             piggybank_goal=piggybank_goal,
                             fee=uint64(fee),
                             cash_out_puzzlehash=cash_out_puzzlehash)

    wallet_id = (await rpc.wallet_client.get_wallets())[0]["id"]
    return piggy_spend, wallet_id


@cli.command(help="Creates piggy bank")
@click.argument("fee")
@click.argument("piggybank_goal")
@coroutine
async def create_piggybank(fee: uint64, piggybank_goal: uint64):
    async with await Rpc.create_connection(hostname, node_port, wallet_port, DEFAULT_ROOT_PATH, chia_config) as rpc:
        piggy_spend, wallet_id = await get_objects(rpc, fee, piggybank_goal)
        pb_puzhash = await piggy_spend.piggybank_puzzlehash()
        pb_coinrecords = await get_piggybank_unspents(rpc, pb_puzhash)
        if not pb_coinrecords:
            logging.info("Creating new piggybank")
            pb_address = await piggy_spend.piggybank_address(current_net)
            await rpc.wallet_client.send_transaction(wallet_id, uint64(1), pb_address)
            while not pb_coinrecords:
                logging.info("Waiting... (piggybank creation)")
                await asyncio.sleep(2)
                pb_coinrecords = await get_piggybank_unspents(rpc, pb_puzhash)
        logging.info(f"Created piggy bank:\n{pb_coinrecords}")


@cli.command(help="Contribute to the Piggy bank")
@click.argument("amount")
@click.argument("fee")
@click.argument("piggybank_goal")
@coroutine
async def contribute(amount: uint64, fee: uint64, piggybank_goal: uint64):
    async with await Rpc.create_connection(hostname, node_port, wallet_port, DEFAULT_ROOT_PATH, chia_config) as rpc:
        if int(amount) - int(fee) <= 0:
            raise Exception("Increase contribution amount, please.")
        piggy_spend, wallet_id = await get_objects(rpc, fee, piggybank_goal)
        contrib_tx: TransactionRecord = await rpc.wallet_client.send_transaction(
            wallet_id=wallet_id,
            amount=uint64(amount),
            fee=10,
            address=await piggy_spend.contrib_address(current_net)
        )
        while True:
            logging.info("Waiting for contribution coin confirm...")
            await asyncio.sleep(2)
            tx: TransactionRecord = await rpc.wallet_client.get_transaction(wallet_id, contrib_tx.name)
            if tx.confirmed:
                contrib_coin: Coin = await filter_contrib_coin(piggy_spend, tx.additions)
                pb_coinrecord: CoinRecord = await get_piggybank_coinrecord(rpc, piggy_spend, wallet_id)
                pb_coin = pb_coinrecord.coin
                sb: SpendBundle = await piggy_spend.get_spend_bundle(pb_coin, contrib_coin)
                res = await rpc.full_node_client.push_tx(sb)
                logging.info(f"Spend bundle:\n{sb}")
                logging.info(f"Result:\n{res}")
                break


async def get_piggybank_unspents(rpc: Rpc, pb_puzhash: bytes32):
    return await rpc.full_node_client.get_coin_records_by_puzzle_hash(pb_puzhash, include_spent_coins=False)


async def get_piggybank_coinrecord(rpc: Rpc, piggy_spend: PiggySpend, wallet_id: str, start_amount: int = 1):
    pb_puzhash = await piggy_spend.piggybank_puzzlehash()
    pb_coinrecords = await get_piggybank_unspents(rpc, pb_puzhash)
    if not pb_coinrecords:
        logging.info("Creating new piggybank")
        pb_address = await piggy_spend.piggybank_address(current_net)
        await rpc.wallet_client.send_transaction(wallet_id, uint64(start_amount), pb_address)
        while not pb_coinrecords:
            logging.info("Waiting... (piggybank creation)")
            await asyncio.sleep(2)
            pb_coinrecords = await get_piggybank_unspents(rpc, pb_puzhash)
    return pb_coinrecords[0]


async def filter_contrib_coin(piggy_spend, additions) -> Coin:
    contrib_puzzlehash = await piggy_spend.contrib_puzzlehash()
    for coin in additions:
        if coin.puzzle_hash == contrib_puzzlehash:
            return coin
    raise ValueError("Couldn't find existing contribution coin")


def set_logger():
    level = logging.INFO
    fmt = '[%(levelname)s] %(asctime)s - %(message)s'
    logging.basicConfig(level=level, format=fmt)


async def main():
    async with await Rpc.create_connection(hostname, node_port, wallet_port, DEFAULT_ROOT_PATH, chia_config) as rpc:
        pass


if __name__ == '__main__':
    set_logger()
    cli(obj={})
    # todo zavolat metody podle zadanych argumentu..
