# Chia Adventures

Chia Adventures je semetrální projekt z předmětu KIV/ZSWI, jehož cílem je prozkoumat blockchainovou síť chia, implementovat na ní jednoduchý smart contract (v Chia světě jsou smart contracty nazývány jako smart coiny) a provést za pomocí zmíněného smart contractu metriku sítě.

![Chia Network logo](https://s.yimg.com/uu/api/res/1.2/2ZGjV0RIRronwewQKZ1UJw--~B/aD0yMzI7dz02MDA7YXBwaWQ9eXRhY2h5b24-/https://media.zenfs.com/en/news_direct/ce6147fdd35ccf00e11e44ec8b05fcdb)


## ⚙️ Instalace
*Předpokládá se, že uživatel má Python verze 3.5<=*
1. `git clone https://gitlab.com/n3399/chia-adventures.git`
2. `pip install pipenv`
3. `pipenv shell`
4. `pipenv install`


## Nastartování uzlu
*Aby aplikace mohla fungovat, musí na uživatelském počítači běžet instance uzlu chia sítě a instance peněženky*
1. `chia init`
2. `chia start node`
3. `chia start wallet`
- Pro vypsání informací o stavu uzlu slouží příkaz: `chia show -s`
- Pro vypsání stavu peněženky slouží příkaz: `chia wallet show`

## 🪙 Smart coin
| :exclamation:  Disclaimer   |
|-----------------------------------------|
Za bezpečnost chytré mince neručíme.

Jako smart coin jsme zvolili známý chia entry level smart contract s názvem "Piggybank", volně přeloženo jako "kasička". Jak již název napovídá, funkcionalita chytré mince je vcelku jednoduchá - uživatel si při jejím vytvoření nadefinuje, při jaké částce se mu "kasička rozbije". Po úspěšném vytvoření kasičky do ní začne posílat peníze a v moment, kdy částka uvnitř kasičky dosáhne definovaného maxima - z kasičky se vyberou všechny peníze a pošlou se na předem definovanou adresu.
#### Příkazy
- `cli_main.py create-piggybank <fee> <max_amount>`
    - Vytvoří instanci chytré mince s poplatkem **fee** a maximální hodnotou **max_amount**
- `cli_main.py contribute <amount> <fee> <max_amount>` 
    - Pošle peníze o hodnotě **amount** do piggy bank s atributy **fee** a **max_amount**
- `cli_main.py search-coin <puzzle_hash>`
    - Najde v síti minci podle jejího **puzzle_hash** 

## 📊 Měření
Všechna měření se ukládájí do SQLite databáze, která je uložená na cestě: `collector/blockchain_metrics.db`
Odkaz na stažení databáze se sample vzorkem měření: [click me!](https://zcu365-my.sharepoint.com/:u:/g/personal/bidlako_office365_zcu_cz/EaqrkvZmpZ1KpYt_8zGj-sQBjKGL86U_ZQEirfbE-SzHsA?e=KZHTfn)

Měření se pouští pomocí skriptu:
`benchmark.py`<br /> 
Na síti jsme měřili celkem tři jevy.
1. Doba vytvoření chytré mince 
2. Hledání mince podle identifikátoru
3. Změna / doplnění obsahu chytré mince
#### Vizualizace měření
Výsledky měření jsou vizualizovány za pomocí knihovny Streamlit. 
Vizualizace se spustí pomocí příkazu:
- `streamlit run gui_main.py`


## Zadání
Úkolem zadání bude vytvořit prototyp aplikace pro zápis záznamů do blockchain sítě Chia. Aplikace musí umožnit do sítě Chia zapsat záznam (64 znaků dlouhý řetězec) a vrátit ID tohoto záznamu. Dále na základě vstupního ID záznamu se aplikace pokusí záznam nalézt a na výstup vypíše, zda by záznam nalezen či nikoliv. Další funkcionalitou bude možnost existující záznam doplnit.

Součástí zadání je provedení statisticky významného (dostatečný počet opakování) experimentu s dobou trvání zápisu, jeho nalezení a úpravou. Tyto experimenty musí být provedeny jak v testovacím prostředí (testnet), tak v ostrém prostředí (mainnet). Pro experimenty v ostrém prostředí bude řešitelům poskytnuto malé množství měny Chia.

