import asyncio
import logging
import time
import copy

from pathlib import Path

from chia.types.blockchain_format.coin import Coin
from chia.consensus.block_record import BlockRecord
from chia.types.blockchain_format.sized_bytes import bytes32
from chia.types.coin_record import CoinRecord
from chia.types.spend_bundle import SpendBundle
from chia.util.ints import uint64
from chia.util.config import load_config
from chia.util.default_root import DEFAULT_ROOT_PATH
from chia.wallet.transaction_record import TransactionRecord

from connector import Rpc

from contracts.drivers.helpers.sign_contribution import AggregatedSignature
from contracts.drivers.helpers.piggy_spend import PiggySpend

from utils.result_wrappers import SearchRecord, ConfirmRecord, AdditionRecord

from collector import Model
from chia.util.bech32m import decode_puzzle_hash

current_net = "xch"


async def wait_for_piggybank_by_id(rpc: Rpc, id: bytes32):
    pb_coin = None
    while not pb_coin:
        logging.info("Waiting... (piggybank search)")
        await asyncio.sleep(2)
        pb_coin = await rpc.full_node_client.get_coin_record_by_name(id)
    return True


async def get_piggybank_unspents(rpc: Rpc, pb_puzhash: bytes32):
    return await rpc.full_node_client.get_coin_records_by_puzzle_hash(pb_puzhash, include_spent_coins=False)


async def get_piggybank_coinrecord(rpc: Rpc, piggy_spend: PiggySpend, wallet_id: str, start_amount: int = 1):
    pb_puzhash = await piggy_spend.piggybank_puzzlehash()
    pb_coinrecords = await get_piggybank_unspents(rpc, pb_puzhash)
    if not pb_coinrecords:
        logging.info("Creating new piggybank")
        pb_address = await piggy_spend.piggybank_address(current_net)
        await rpc.wallet_client.send_transaction(wallet_id, uint64(start_amount), pb_address)
        while not pb_coinrecords:
            logging.info("Waiting... (piggybank creation)")
            await asyncio.sleep(2)
            pb_coinrecords = await get_piggybank_unspents(rpc, pb_puzhash)
    return pb_coinrecords[0]


async def filter_contrib_coin(piggy_spend, additions) -> Coin:
    contrib_puzzlehash = await piggy_spend.contrib_puzzlehash()
    for coin in additions:
        if coin.puzzle_hash == contrib_puzzlehash:
            return coin
    raise ValueError("Couldn't find existing contribution coin")


def set_logger():
    level = logging.INFO
    fmt = '[%(levelname)s] %(asctime)s - %(message)s'
    logging.basicConfig(filename="logfile.log",
                        filemode="w",
                        format=fmt,
                        level=level)


async def run_measure(iters):
    set_logger()
    chia_config = load_config(Path(DEFAULT_ROOT_PATH), "config.yaml")
    hostname = chia_config["farmer"]["full_node_peer"]["host"]
    node_port = chia_config["full_node"]["rpc_port"]
    wallet_port = chia_config["wallet"]["rpc_port"]

    async with await Rpc.create_connection(hostname, node_port, wallet_port, DEFAULT_ROOT_PATH, chia_config) as rpc:
        wallet_id = (await rpc.wallet_client.get_wallets())[0]["id"]
        wallet_fingerprint = await rpc.wallet_client.get_logged_in_fingerprint()
        keys = await rpc.wallet_client.get_private_key(wallet_fingerprint)
        public_key = keys['pk']
        private_key = keys['sk']
        aggregator = AggregatedSignature(private_key, public_key)

        piggybank_goal = 1_000_000_000
        fee = 30
        contrib_min_amount = 100 + fee

        # Important note: chia wallets randomize their wallet addresses after transaction, to prevent
        # randomized piggybank puzzlehashes (we want to keep track of them), we will hard code cash out
        # puzzlehash (can be wallet, coin, etc..).
        # <TODO Put your wallet address here:>
        cash_out_address = "xch1u06mrmypa54jhyh7zwz6hy6cl0uvxgu8uze5yaj3jmwp9hq52mlsguptqk"
        # <TODO End wallet address>
        cash_out_puzzlehash: bytes32 = decode_puzzle_hash(cash_out_address)

        piggy_spend = PiggySpend(rpc=rpc,
                                 aggregator=aggregator,
                                 piggybank_goal=piggybank_goal,
                                 fee=fee,
                                 cash_out_puzzlehash=cash_out_puzzlehash)

        pb_coinrecord: CoinRecord = await get_piggybank_coinrecord(rpc, piggy_spend, wallet_id)
        pb_coin = pb_coinrecord.coin

        for i in range(iters):
            logging.info(f"New iteration: {i}, {iters - i} / {iters}")
            contrib_tx: TransactionRecord = await rpc.wallet_client.send_transaction(
                wallet_id=wallet_id,
                amount=uint64(contrib_min_amount),
                address=await piggy_spend.contrib_address(current_net)
            )
            logging.info("Will be waiting for transaction confirm...")
            while True:
                search_start = time.time()
                search_mem = len(await rpc.full_node_client.get_all_mempool_tx_ids())
                tx: TransactionRecord = await rpc.wallet_client.get_transaction(wallet_id, contrib_tx.name)
                search_end = time.time()
                if tx.confirmed:
                    with Model() as db:
                        block: BlockRecord = await rpc.full_node_client.get_block_record_by_height(
                            tx.confirmed_at_height)
                        confirm_dur = abs(tx.created_at_time - block.timestamp)
                        print(f"Confirm duration {confirm_dur}")
                        db.insert_confirm(
                            ConfirmRecord(tx.created_at_time, tx.fee_amount, confirm_dur, tx.name,
                                          tx.confirmed_at_height, search_mem))

                        search_dur = search_end - search_start
                        print(f"Search duration {search_dur}")
                        db.insert_search(SearchRecord(tx.created_at_time, search_dur, tx.name))
                        contrib_coin = await filter_contrib_coin(piggy_spend, tx.additions)
                        sb: SpendBundle = await piggy_spend.get_spend_bundle(pb_coin, contrib_coin)
                        logging.info(f"SPEND BUNDLE:\n{sb}")

                        push_start = time.time()
                        add_mem = len(await rpc.full_node_client.get_all_mempool_tx_ids())
                        res = await rpc.full_node_client.push_tx(sb)
                        logging.info(f"Response: {res}")

                        old_pb_coin = copy.deepcopy(pb_coin)
                        pb_coin = sb.additions()[0]
                        if await wait_for_piggybank_by_id(rpc, pb_coin.name()):
                            change_dur = time.time() - push_start
                            print(f"Change duration {change_dur}")
                            db.insert_addition(
                                AdditionRecord(int(push_start), change_dur, old_pb_coin.name(), pb_coin.name(),
                                               add_mem))
                            break


if __name__ == '__main__':
    asyncio.run(run_measure(iters=100_000))
