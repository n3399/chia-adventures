import os

from chia.types.blockchain_format.coin import Coin
from chia.types.blockchain_format.program import Program
from chia.types.blockchain_format.sized_bytes import bytes32
from chia.types.coin_spend import CoinSpend
from chia.types.spend_bundle import SpendBundle
from chia.util.bech32m import encode_puzzle_hash
from chia.util.ints import uint64

from connector import Rpc
from contracts.drivers import get_coin_hash

from contracts.drivers.helpers.sign_contribution import AggregatedSignature
from contracts.drivers.piggybank import (
    solution_for_piggybank,
    create_piggybank_puzzle
)
from contracts.drivers.contribution import (
    solution_for_contrib,
    create_contrib_puzzle
)

os_sep = os.sep
json_skeleton = f".{os.sep}spends{os.sep}spend_bundle_skeleton.json"
sp_temp_path = f".{os.sep}temp{os.sep}temp_sb.json"


class PiggySpend:
    aggregator: AggregatedSignature
    rpc: Rpc
    piggybank_goal: uint64
    cash_out_puzhash: bytes32

    def __init__(self, rpc: Rpc, aggregator: AggregatedSignature, piggybank_goal: int, fee: int, cash_out_puzzlehash: bytes32):
        self.rpc = rpc
        self.aggregator = aggregator
        self.piggybank_goal = uint64(piggybank_goal)
        self.fee = fee
        self.cash_out_puzhash = cash_out_puzzlehash

    async def get_spend_bundle(self, pb_coin: Coin, contrib_coin: Coin):
        contrib_amount = contrib_coin.amount - uint64(self.fee)
        if contrib_amount <= 0:
            raise ValueError(
                f"Insufficient amount: contribution amount is {contrib_amount} and fee is set to {self.fee}")
        tx_spend_bundle = SpendBundle(
            [
                CoinSpend(
                    pb_coin,
                    await self.piggybank_program(),
                    solution_for_piggybank(pb_coin, contrib_amount)
                ),
                CoinSpend(
                    contrib_coin,
                    await self.contrib_program(),
                    solution_for_contrib(pb_coin, contrib_amount)
                )
            ],
            await self.get_agg_sig(pb_coin, contrib_amount)
        )
        return tx_spend_bundle

    async def get_agg_sig(self, pb_coin: Coin, contrib_amount: int):
        coin_id = get_coin_hash(pb_coin)
        new_amount = contrib_amount + pb_coin.amount
        return self.aggregator.get_agg_sig(coin_id, new_amount)

    async def piggybank_program(self) -> Program:
        return create_piggybank_puzzle(uint64(self.piggybank_goal), self.cash_out_puzhash)

    async def piggybank_puzzlehash(self) -> bytes32:
        pb_puzzle = await self.piggybank_program()
        return pb_puzzle.get_tree_hash()

    async def piggybank_address(self, prefix: str = "txch") -> str:
        pb_puzhash = await self.piggybank_puzzlehash()
        return encode_puzzle_hash(pb_puzhash, prefix)

    async def contrib_program(self) -> Program:
        return create_contrib_puzzle(self.aggregator.PUBKEY, self.fee)

    async def contrib_puzzlehash(self) -> bytes32:
        contrib_puzzle = await self.contrib_program()
        return contrib_puzzle.get_tree_hash()

    async def contrib_address(self, prefix: str = "txch") -> str:
        contrib_puzhash = await self.contrib_puzzlehash()
        return encode_puzzle_hash(contrib_puzhash, prefix)
