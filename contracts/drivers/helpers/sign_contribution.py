from blspy import PrivateKey, AugSchemeMPL, PublicKeyMPL, G2Element
from chia.types.blockchain_format.sized_bytes import bytes32

from chia.util.hash import std_hash

from clvm.casts import int_to_bytes


class AggregatedSignature:
    __SK: PrivateKey
    PUBKEY: str

    def __init__(self, SK: str, PB: str):
        self.__SK = PrivateKey.from_bytes(bytes.fromhex(SK))
        self.PUBKEY = PB

    def verify(self, message: str, signature: G2Element):
        pk = PublicKeyMPL.from_bytes(bytes.fromhex(self.PUBKEY))
        return AugSchemeMPL.verify(pk, message, signature)

    def get_agg_sig(self, pb_coin_id: bytes32, new_amount: int):
        NEW_AMOUNT = int_to_bytes(new_amount)
        signature: AugSchemeMPL = AugSchemeMPL.sign(self.__SK, std_hash(pb_coin_id + NEW_AMOUNT))
        return signature
