from chia.types.blockchain_format.coin import Coin
from chia.types.blockchain_format.sized_bytes import bytes32
from chia.util.hash import std_hash
from clvm.casts import int_to_bytes


def get_coin_hash(coin: Coin) -> bytes32:
    return _get_coin_hash(coin.parent_coin_info, coin.puzzle_hash, coin.amount)


def _get_coin_hash(parent_coin_info: bytes32, puzzle_hash: bytes32, amount: int) -> bytes32:
    return std_hash(parent_coin_info + puzzle_hash + int_to_bytes(amount))
