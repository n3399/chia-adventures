from chia.types.blockchain_format.coin import Coin
from chia.types.blockchain_format.program import Program

from cdv.util.load_clvm import load_clvm
from chia.util.ints import uint64

from contracts.drivers.coin_hash import get_coin_hash

CONTRIBUTION_MOD: Program = load_clvm("contrib.clsp", "puzzle")


def create_contrib_puzzle(pubkey: str, fee: int) -> Program:
    PUBKEY = bytes.fromhex(pubkey)
    return CONTRIBUTION_MOD.curry(PUBKEY, fee)


def solution_for_contrib(pb_coin: Coin, contrib_amount: uint64) -> Program:
    pb_coin_id = get_coin_hash(pb_coin)
    return Program.to([pb_coin_id, contrib_amount + pb_coin.amount])
