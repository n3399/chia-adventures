from chia.rpc.full_node_rpc_client import FullNodeRpcClient
from chia.rpc.wallet_rpc_client import WalletRpcClient


class Rpc:
    full_node_client: FullNodeRpcClient
    wallet_client: WalletRpcClient
    tasks: []
    state: {}
    hostname: str
    node_port: int
    wallet_port: int

    @classmethod
    async def create_connection(cls, hostname, node_port, wallet_port, root_path, net_config):
        self = cls()
        try:
            self.hostname = hostname
            self.node_port = node_port
            self.wallet_port = wallet_port
            self.root_path = root_path
            self.net_config = net_config
            self.full_node_client = await FullNodeRpcClient().create(hostname, node_port, root_path, net_config)
            self.wallet_client = await WalletRpcClient().create(hostname, wallet_port, root_path, net_config)
            return self
        except Exception as ex:
            print("Connection failed")
            await self.close_connection()

    async def close_connection(self):
        self.full_node_client.close()
        self.wallet_client.close()

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.close_connection()
