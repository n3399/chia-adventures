from chia.types.blockchain_format.sized_bytes import bytes32
from chia.util.ints import uint32


class Record:

    def __init__(self, timestamp: int, duration: float):
        self.timestamp: int = timestamp
        self.duration: float = duration


class AdditionRecord(Record):
    def __init__(self, timestamp: int, duration: float, coin_name_old: bytes32, coin_name_new: bytes32, mempool: int):
        super().__init__(timestamp, duration)
        self.coin_name_old: bytes32 = coin_name_old
        self.coin_name_new: bytes32 = coin_name_new
        self.mempool = mempool


class SearchRecord(Record):
    def __init__(self, timestamp: int, duration: float, coin_name: bytes32):
        super().__init__(timestamp, duration)
        self.coin_name: bytes32 = coin_name


class ConfirmRecord(Record):

    def __init__(self, timestamp: int, fee: int, duration: float, tx_id: bytes32, block_height: uint32, mempool: int):
        super().__init__(timestamp, duration)
        self.tx_id: bytes32 = tx_id
        self.fee: int = fee
        self.mempool: int = mempool
        self.block_height: int = block_height
