import pandas as pd
import streamlit as st
import plotly.express as px
from streamlit_option_menu import option_menu
from PIL import Image
from collector import Model
from pathlib import Path


LOGO_PATH = "resources/chia_logo.png"
MENU_IMG_PATH = "resources/chia.png"


def load_images():
    logo = Image.open(LOGO_PATH)
    menu_img = Image.open(MENU_IMG_PATH)
    return logo, menu_img


def config_page(logo):
    st.set_page_config(
        page_title="Chia",
        page_icon=logo,
        layout="wide",
    )


def create_plot():
    with Model() as db:
        tran_df = db.get_confirm_df()
        dur_df = db.get_search_df()
        add_df = db.get_addition_df()

    tran_df["duration"] = tran_df["duration"].abs()

    df = pd.DataFrame()
    df["tran_duration"] = tran_df["duration"]
    df["search_duration"] = dur_df["duration"]
    df["change_duration"] = add_df["duration"]

    st.subheader("Transaction confirm duration and mempool size")
    fig = px.scatter(tran_df, x="duration", y="mempool", trendline="lowess", trendline_options=dict(frac=0.1),
                     trendline_color_override="red")
    st.plotly_chart(fig, use_container_width=True)

    st.subheader("Addition confirm duration and mempool size")
    fig = px.scatter(add_df, x="duration", y="mempool", trendline="lowess", trendline_options=dict(frac=0.1),
                     trendline_color_override="red")
    st.plotly_chart(fig, use_container_width=True)

    st.subheader("Record search duration")
    st.line_chart(dur_df.get("duration"))





def create_menu(menu_img):
    st.sidebar.image(menu_img, use_column_width=True, caption="\n\n")
    with st.sidebar:
        selected = option_menu(
            menu_title="Menu",
            options=["Home", "Metrics"],
            icons=["house", "graph-up"],
            styles={
                "icon": {"font-size": "25px"}
            }
        )

    return selected


def create_body(selected):
    if selected == "Home":
        st.header("Home")
        st.markdown(Path("README.md").read_text(encoding="utf-8"))

    elif selected == "Metrics":
        st.header("Network Metrics")
        create_plot()


logo, menu_img = load_images()
config_page(logo)
selected = create_menu(menu_img)
create_body(selected)


def main():
    pass


if __name__ == "__main__":
    print("View started")
